package com.mvirdaeus.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

public class StandingsActivity extends AppCompatActivity {

    ArrayAdapter<String> displayAdapter_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standings);

        Intent intent = getIntent();
        ArrayList<String> scoreStrings = new ArrayList<>();
        
        ArrayList<Score> scores = intent.getParcelableArrayListExtra("SCORES");
        if(scores != null) {

            Collections.sort(scores, Score.byScoreHighest);
            for(int i = 0; i < scores.size(); ++i) {
                scoreStrings.add(scores.get(i).name + ": " + scores.get(i).score + " wins");
            }

            displayAdapter_ = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, scoreStrings);
            ListView listView = findViewById(R.id.lv_scores);
            listView.setAdapter(displayAdapter_);
        }
    }

    public void closeActivity(View view) {
        finish();
    }
}