package com.mvirdaeus.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NameEntryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_entry);
    }

    public void saveAndClose(View view) {
        EditText nameEntry = findViewById(R.id.et_nameField);
        String name = nameEntry.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("PLAYER_NAME", name);
        setResult(MainActivity.NAME_ENTRY, intent);
        finish();
    }
}