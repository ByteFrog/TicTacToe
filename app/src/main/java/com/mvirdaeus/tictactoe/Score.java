package com.mvirdaeus.tictactoe;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

public class Score implements Parcelable {
    public String name;
    public int score;

    public Score(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public Score(Parcel in) {
        this.name = in.readString();
        this.score = in.readInt();
    }

    public static Comparator<Score> byScoreHighest = new Comparator<Score>() {
        @Override
        public int compare(Score o1, Score o2) {
            return o2.score - o1.score;
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.score);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Object createFromParcel(Parcel source) {
            return new Score(source);
        }

        @Override
        public Object[] newArray(int size) {
            return new Score[size];
        }
    };
}
