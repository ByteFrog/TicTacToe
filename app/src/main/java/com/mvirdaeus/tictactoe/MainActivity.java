package com.mvirdaeus.tictactoe;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final int NAME_ENTRY = 1;
    public static final String ACTION_ADD_WIN = "com.mvirdaeus.tictactoe.action.ADD_WIN";

    private String playerName_;
    private ArrayList<Score> scores_;

    private BroadcastReceiver receiver_ = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("TAG", "onReceive: Broadcast received.");
            if(intent.getAction().equals(ACTION_ADD_WIN)) {
                String winner = intent.getStringExtra("WINNER");
                Log.d("TAG", "onReceive: Got winner " + winner);
                addWin(winner);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerName_ = "Player";
        scores_ = new ArrayList<>();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver_, new IntentFilter(ACTION_ADD_WIN));
    }

    @Override
    protected void onDestroy() {
        if(receiver_ != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver_);
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case NAME_ENTRY:
                playerName_ = data.getStringExtra("PLAYER_NAME");
                Toast.makeText(getApplicationContext(), "Welcome " + playerName_, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showNameEntry(View view) {
        Intent intent = new Intent(this, NameEntryActivity.class);
        startActivityForResult(intent, NAME_ENTRY);
    }

    public void launchGameVsCpu(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("PLAYER_NAME", playerName_);
        intent.putExtra("VS_CPU", true);
        startActivity(intent);
    }

    public void launchGameVsPlayer(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("VS_CPU", false);
        startActivity(intent);
    }

    public void showStandings(View view) {
        Intent intent = new Intent(this, StandingsActivity.class);
        intent.putParcelableArrayListExtra("SCORES", scores_);
        startActivity(intent);
    }

    public void addWin(String name) {
        int i;
        for(i = 0; i < scores_.size(); ++i) {
            if(name.equals(scores_.get(i).name)) {
                break;
            }
        }
        if(i == scores_.size()) {
            scores_.add(new Score(name, 1));
        }
        else {
            scores_.get(i).score += 1;
        }
    }
}