package com.mvirdaeus.tictactoe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.Random;
import java.util.Vector;

public class GameActivity extends AppCompatActivity implements DialogInterface.OnClickListener {

    private String player_;

    Button[][] buttons_;
    char[][] gameBoard_;
    TextView turnDisplay_;

    private static final char STATE_PLAYER1_TURN = 0;
    private static final char STATE_PLAYER2_TURN = 1;
    private static final char STATE_CPU_TURN = 2;
    private static final char STATE_END = 3;

    char gameState_;
    boolean versusCpu_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        turnDisplay_ = findViewById(R.id.tv_turnDisplay);

        //First index column, second index row (x, y)
        buttons_ = new Button[3][3];
        buttons_[0][0] = findViewById(R.id.button0);
        buttons_[1][0] = findViewById(R.id.button1);
        buttons_[2][0] = findViewById(R.id.button2);
        buttons_[0][1] = findViewById(R.id.button3);
        buttons_[1][1] = findViewById(R.id.button4);
        buttons_[2][1] = findViewById(R.id.button5);
        buttons_[0][2] = findViewById(R.id.button6);
        buttons_[1][2] = findViewById(R.id.button7);
        buttons_[2][2] = findViewById(R.id.button8);

        buttons_[0][0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(0, 0);
            }
        });

        buttons_[1][0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(1, 0);
            }
        });

        buttons_[2][0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(2, 0);
            }
        });

        buttons_[0][1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(0, 1);
            }
        });

        buttons_[1][1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(1, 1);
            }
        });

        buttons_[2][1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(2, 1);
            }
        });

        buttons_[0][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(0, 2);
            }
        });

        buttons_[1][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(1, 2);
            }
        });

        buttons_[2][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPlayerTurn(2, 2);
            }
        });

        Intent intent = getIntent();
        versusCpu_ = intent.getBooleanExtra("VS_CPU", true);
        if(versusCpu_) {
            player_ = intent.getStringExtra("PLAYER_NAME");
        }

        gameState_ = STATE_PLAYER1_TURN;

        gameBoard_ = new char[3][3];
        for(int x = 0; x < 3; ++x) {
            for(int y = 0; y < 3; ++y) {
                gameBoard_[x][y] = ' ';
            }
        }

        updateDisplay();
    }

    private void updateDisplay() {
        for(int x = 0; x < 3; ++x) {
            for(int y = 0; y < 3; ++y) {
                buttons_[x][y].setText(String.valueOf(gameBoard_[x][y]));
            }
        }

        switch(gameState_) {
            case STATE_PLAYER1_TURN:
                if(versusCpu_) {
                    turnDisplay_.setText(getString(R.string.current_turn, player_));
                }
                else {
                    turnDisplay_.setText(getString(R.string.current_turn, getString(R.string.player_1)));
                }
                break;
            case STATE_PLAYER2_TURN:
                turnDisplay_.setText(getString(R.string.current_turn, getString(R.string.player_2)));
                break;
            case STATE_CPU_TURN:
                turnDisplay_.setText(getString(R.string.current_turn, getString(R.string.cpu)));
                break;
        }
    }

    private void doPlayerTurn(int x, int y) {
        if(gameState_ == STATE_PLAYER1_TURN || gameState_ == STATE_PLAYER2_TURN) {

            if(gameBoard_[x][y] != ' ') {
                Toast.makeText(this, "Space occupied", Toast.LENGTH_SHORT).show();
                return;
            }

            if(gameState_ == STATE_PLAYER1_TURN) {
                gameBoard_[x][y] = 'X';
            }
            else {
                gameBoard_[x][y] = 'O';
            }
            updateDisplay();

            new AdvanceTurnTask(this).execute();
        }
    }

    private void reset() {
        for(int x = 0; x < 3; ++x) {
            for(int y = 0; y < 3; ++y) {
                gameBoard_[x][y] = ' ';
            }
        }

        gameState_ = STATE_PLAYER1_TURN;
        updateDisplay();
    }

    private void showEndGameDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.play_again, this);
        builder.setNegativeButton(R.string.main_menu, this);
        builder.create().show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which) {
            case DialogInterface.BUTTON_NEGATIVE:
                finish();
                break;
            case DialogInterface.BUTTON_POSITIVE:
                reset();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putCharArray("ROW_1", gameBoard_[0]);
        outState.putCharArray("ROW_2", gameBoard_[1]);
        outState.putCharArray("ROW_3", gameBoard_[2]);
        outState.putChar("GAME_STATE", gameState_);
        outState.putBoolean("VERSUS_CPU", versusCpu_);
        outState.putString("PLAYER_NAME", player_);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        gameBoard_[0] = savedInstanceState.getCharArray("ROW_1");
        gameBoard_[1] = savedInstanceState.getCharArray("ROW_2");
        gameBoard_[2] = savedInstanceState.getCharArray("ROW_3");
        gameState_ = savedInstanceState.getChar("GAME_STATE");
        versusCpu_ = savedInstanceState.getBoolean("VERSUS_CPU");
        player_ = savedInstanceState.getString("PLAYER_NAME");

        updateDisplay();
    }

    private class AdvanceTurnTask extends AsyncTask<Void, Character, Character> {

        private WeakReference<GameActivity> activityReference_;

        AdvanceTurnTask(GameActivity context) {
            activityReference_ = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Character doInBackground(Void... voids) {
            char victor = checkVictory();
            if(victor != ' ') {
                return victor;
            }

            if(versusCpu_) {
                //CPU turn, if the player didn't win.
                publishProgress(STATE_CPU_TURN);
                doCpuTurn();
                victor = checkVictory();
            }

            return victor;
        }

        @Override
        protected void onProgressUpdate(Character... values) {
            super.onProgressUpdate(values);
            GameActivity activity = activityReference_.get();
            if(activity == null || activity.isFinishing()) return;

            activity.gameState_ = STATE_CPU_TURN;
            activity.updateDisplay();
        }

        @Override
        protected void onPostExecute(Character victor) {
            GameActivity activity = activityReference_.get();
            if(activity == null || activity.isFinishing()) return;

            activity.updateDisplay();

            if(victor == ' ') {
                if(versusCpu_) {
                    activity.gameState_ = STATE_PLAYER1_TURN;
                }
                else {
                    if(gameState_ == STATE_PLAYER1_TURN) {
                        gameState_ = STATE_PLAYER2_TURN;
                    }
                    else {
                        gameState_ = STATE_PLAYER1_TURN;
                    }
                }
            }
            else {
                if(versusCpu_) {
                    activity.gameState_ = STATE_END;
                    String message = "";
                    if (victor != 'D') {
                        Intent intent = new Intent(MainActivity.ACTION_ADD_WIN);
                        if (victor == 'X') {
                            intent.putExtra("WINNER", activity.player_);
                            message = getString(R.string.declare_winner, activity.player_);
                        } else if (victor == 'O') {
                            intent.putExtra("WINNER", getString(R.string.cpu));
                            message = getString(R.string.declare_winner, getString(R.string.cpu));
                        }
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
                    } else {
                        message = getString(R.string.declare_draw);
                    }
                    showEndGameDialog(message);
                }
                else {
                    String message = "";
                    if(victor != 'D') {
                        if(victor == 'X') {
                            message = getString(R.string.declare_winner, getString(R.string.player_1));
                        }
                        else {
                            message = getString(R.string.declare_winner, getString(R.string.player_2));
                        }
                    }
                    else {
                        message = getString(R.string.declare_draw);
                    }
                    showEndGameDialog(message);
                }
            }

            super.onPostExecute(victor);
        }

        protected char checkVictory() {
            //Returns 'X' or 'O' if either has three in a row, ' ' if no victor was found
            //or 'D' if the game was a draw.

            char victor = ' ';

            //Check columns
            for(int x = 0; x < 3 && victor == ' '; ++x) {
                //Check if the first entry is empty to avoid declaring victory for 3 blank spaces in a row.
                if(gameBoard_[x][0] != ' ') {
                    if(gameBoard_[x][0] == gameBoard_[x][1] && gameBoard_[x][0] == gameBoard_[x][2]) {
                        victor = gameBoard_[x][0];
                    }
                }
            }

            //Check rows
            for(int y = 0; y < 3 && victor == ' '; ++y) {
                //Check if the first entry is empty to avoid declaring victory for 3 blank spaces in a row.
                if(gameBoard_[0][y] != ' ') {
                    if(gameBoard_[0][y] == gameBoard_[1][y] && gameBoard_[0][y] == gameBoard_[2][y]) {
                        victor = gameBoard_[0][y];
                    }
                }
            }

            //Check diagonals
            if(gameBoard_[0][0] != ' ' && victor == ' ') {
                if(gameBoard_[0][0] == gameBoard_[1][1] && gameBoard_[0][0] == gameBoard_[2][2]) {
                    victor = gameBoard_[0][0];
                }
            }

            if(gameBoard_[0][2] != ' ' && victor == ' ') {
                if(gameBoard_[0][2] == gameBoard_[1][1] && gameBoard_[0][2] == gameBoard_[2][0]) {
                    victor = gameBoard_[0][2];
                }
            }

            //If no victor was found, check for a draw.
            if(victor == ' ') {
                boolean boardFull = true;
                for(int x = 0; x < 3 && boardFull; ++x) {
                    for(int y = 0; y < 3 && boardFull; ++y) {
                        if(gameBoard_[x][y] == ' ') {
                            boardFull = false;
                        }
                    }
                }
                if(boardFull) {
                    victor = 'D';
                }
            }

            return victor;
        }

        protected void doCpuTurn() {
            /* The algorithm for the AI player will be pretty simple:
               1) Check for any winning moves (sequences where only one 'O' is missing).
               2) Check for any blocking moves (sequences where only one 'X' is missing).
               3) If there are no winning or blocking moves, pick a blank spot at random.
             */

            //Winning moves
            int move = checkWinningMoves('O');

            //Blocking moves
            if(move == -1) {
                move = checkWinningMoves('X');
            }

            //Random move
            if(move == -1) {
                Vector<Integer> moves = new Vector<>();
                for(int x = 0; x < 3; ++x) {
                    for(int y = 0; y < 3; ++y) {
                        if(gameBoard_[x][y] == ' ') {
                            moves.add((y * 3) + x);
                        }
                    }
                }
                Random r = new Random();
                move = moves.get(r.nextInt(moves.size()));
            }

            int y = (int) Math.floor((double) move / 3.0);
            int x = move % 3;
            gameBoard_[x][y] = 'O';
        }

        //Returns an empty position that would produce 3 in a column, or -1 if no such position is available.
        private int checkColumn(char symbol, int column) {
            if(gameBoard_[column][0] == ' ' && gameBoard_[column][1] == symbol && gameBoard_[column][2] == symbol) {
                return column;
            }
            if(gameBoard_[column][0] == symbol && gameBoard_[column][1] == ' ' && gameBoard_[column][2] == symbol) {
                return column + 3;
            }
            if(gameBoard_[column][0] == symbol && gameBoard_[column][1] == symbol && gameBoard_[column][2] == ' ') {
                return column + 6;
            }
            return -1;
        }

        //Returns an empty position that would produce 3 in a row, or -1 if no such position is available.
        private int checkRow(char symbol, int row) {
            if(gameBoard_[0][row] == ' ' && gameBoard_[1][row] == symbol && gameBoard_[2][row] == symbol) {
                return (row * 3);
            }
            if(gameBoard_[0][row] == symbol && gameBoard_[1][row] == ' ' && gameBoard_[2][row] == symbol) {
                return (row * 3) + 1;
            }
            if(gameBoard_[0][row] == symbol && gameBoard_[1][row] == symbol && gameBoard_[2][row] == ' ') {
                return (row * 3) + 2;
            }
            return -1;
        }

        //Returns an empty position that would produce 3 in a diagonal from upper left corner (0,0) to lower right corner (2,2)
        //or -1 if no such position exists.
        private int checkDiagonal1(char symbol) {
            if(gameBoard_[0][0] == ' ' && gameBoard_[1][1] == symbol && gameBoard_[2][2] == symbol) {
                return 0;
            }
            if(gameBoard_[0][0] == symbol && gameBoard_[1][1] == ' ' && gameBoard_[2][2] == symbol) {
                return 4;
            }
            if(gameBoard_[0][0] == symbol && gameBoard_[1][1] == symbol && gameBoard_[2][2] == ' ') {
                return 8;
            }
            return -1;
        }

        //Returns an empty position that would produce 3 in a diagonal from upper left corner (0,0) to lower right corner (2,2)
        //or -1 if no such position exists.
        private int checkDiagonal2(char symbol) {
            if(gameBoard_[0][2] == ' ' && gameBoard_[1][1] == symbol && gameBoard_[2][0] == symbol) {
                return 6;
            }
            if(gameBoard_[0][2] == symbol && gameBoard_[1][1] == ' ' && gameBoard_[2][0] == symbol) {
                return 4;
            }
            if(gameBoard_[0][2] == symbol && gameBoard_[1][1] == symbol && gameBoard_[2][0] == ' ') {
                return 2;
            }
            return -1;
        }

        //Returns a position on the board that would produce a victory for the given symbol (X or O)
        //Positions are numbered from left to right, top to bottom, starting at 0.
        private int checkWinningMoves(char symbol) {

            int move = -1;

            for(int x = 0; x < 3 && move == -1; ++x) {
                move = checkColumn(symbol, x);
            }

            for(int y = 0; y < 3 && move == -1; ++y) {
                move = checkRow(symbol, y);
            }

            if(move == -1) {
                move = checkDiagonal1(symbol);
            }

            if(move == -1) {
                move = checkDiagonal2(symbol);
            }

            return move;
        }
    }
}